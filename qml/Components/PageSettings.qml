/*
 */

import QtQuick 2.9
import Lomiri.Components 1.3

import QtQuick.LocalStorage 2.0


Page {
    id: settingsPage
    objectName: "settingsPage"
    anchors.fill: parent

    header: HeaderSettings {
        id: pageHeader
        title: i18n.tr("Settings")
        flickable: settingsFlickable
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: settingsFlickable
    }

    Flickable {
        id: settingsFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height
        topMargin: units.gu(2)
        bottomMargin: units.gu(4)

        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(2)

            Text {
                text: "<b>"+ i18n.tr("Theme") + "</b>"
                color: mainColor
                width:  parent.width - units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ListItem {
                id: themeTF
                width: parent.width
                height: cTheme.height > 0
                    ? cTheme.height
                    : units.gu(7)
                divider.visible: false
                highlightColor: root.highlightColor

                property string themeName

                ChooserTheme {
                    id: cTheme
                    width: parent.width

                    onSelectedThemeChanged: themeTF.themeName = selectedTheme
                }
            }

            Text {
                text: "<b>"+ i18n.tr("Backup and import") + "</b>"
                color: mainColor
                width:  parent.width - units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ListView {
                width: parent.width
                height: units.gu(7) * settingsModel.count
                interactive: false

                model: settingsModel
                delegate: settingsDelegate
            }

            ListModel {
                id: settingsModel

                function initialize() {
                    settingsModel.append({
                        "text": i18n.tr("Manage Backup"),
                        "subText": i18n.tr("Export, import keys and create a back up file"),
                        "page": "PageBackup.qml"
                    });

                    /*
                    settingsModel.append({
                        "text": i18n.tr("Check Database"),
                        "subText": i18n.tr("Try to solve issues with imported key"),
                        "page": "PageCheckdB.qml"
                    });
                    */
                }

                Component.onCompleted: initialize()
            }

            Component {
                id: settingsDelegate

                ListItem {
                    width: parent.width
                    divider.visible: false
                    highlightColor: root.highlightColor

                    ListItemLayout {
                        width: parent.width
                        title.text: text
                        subtitle.text: subText

                        ProgressionSlot {}
                    }

                    onClicked: mainStack.push(Qt.resolvedUrl(page));
                }
            }
        }
    }

    Component.onCompleted: {
        //Load settings from config
    }
}
