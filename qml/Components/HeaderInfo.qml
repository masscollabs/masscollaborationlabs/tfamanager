/*
 */

import QtQuick 2.9
import Lomiri.Components 1.3

HeaderBase {
    signal editKey()

    trailingActionBar {
        actions: [
/*
            Action {
                iconName: "share"
                text: i18n.tr("Share")

                onTriggered: {
                }
            },
*/
            Action {
                iconName: "edit"
                text: i18n.tr("Edit")

                onTriggered: editKey()
            }
       ]
    }
}
