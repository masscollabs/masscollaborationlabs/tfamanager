/*
 */

import QtQuick 2.9
import Lomiri.Components 1.3

HeaderBase {
    trailingActionBar {
        actions: Action {
            iconName: "info"
            text: i18n.tr("Information")

            onTriggered: {
                mainStack.push(Qt.resolvedUrl("../About.qml"));
            }
        }
    }
}
