# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the tfamanager.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: tfamanager.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-19 04:24+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../About.qml.in:37
msgid "About"
msgstr ""

#: ../About.qml.in:91
msgid "Version %1. Source %2"
msgstr ""

#: ../About.qml.in:100
msgid "Under License %1"
msgstr ""

#: ../About.qml.in:109
msgid "App used %1 times. Consider donating: %2"
msgstr ""

#: ../About.qml.in:147
msgid "App Development"
msgstr ""

#: ../About.qml.in:154
msgid "Translate the app"
msgstr ""

#: ../About.qml.in:160
msgid "Code Used from"
msgstr ""

#: ../About.qml.in:166 ../About.qml.in:172
msgid "Icons"
msgstr ""

#: ../About.qml.in:167
msgid "App Icon"
msgstr ""

#: ../About.qml.in:173
msgid "Scan Icon"
msgstr ""

#: ../qml/Components/ChooserTheme.qml:47
msgid "System Theme"
msgstr ""

#: ../qml/Components/DialogDonate.qml:27
msgid "Consider donating"
msgstr ""

#: ../qml/Components/DialogDonate.qml:28
msgid "You have been using this app %1 times."
msgstr ""

#: ../qml/Components/DialogDonate.qml:31
msgid "Donate"
msgstr ""

#: ../qml/Components/DialogDonate.qml:41
#: ../qml/Components/DialogImportResum.qml:32
#: ../qml/Components/HeaderMain.qml:46
msgid "Close"
msgstr ""

#: ../qml/Components/DialogImportResum.qml:7
msgid "Import Summary"
msgstr ""

#: ../qml/Components/DialogImportResum.qml:14
msgid "Keys imported successfully: "
msgstr ""

#: ../qml/Components/DialogImportResum.qml:20
msgid "Keys skipped: "
msgstr ""

#: ../qml/Components/DialogImportResum.qml:26
msgid "Keys with errors: "
msgstr ""

#: ../qml/Components/EmptyDocument.qml:34
msgid "Empty List"
msgstr ""

#: ../qml/Components/EmptyDocument.qml:35
msgid ""
"Please, scan a Qr code with Barcode Reader app and tap 'Open url' or add "
"info manually in the bottom edge menu"
msgstr ""

#: ../qml/Components/HeaderInfo.qml:23 ../qml/Components/PageEditAddKey.qml:26
msgid "Edit"
msgstr ""

#: ../qml/Components/HeaderMain.qml:37
msgid "Search key…"
msgstr ""

#: ../qml/Components/HeaderMain.qml:55 ../qml/Components/PageSettings.qml:17
msgid "Settings"
msgstr ""

#: ../qml/Components/HeaderMain.qml:67
msgid "Search"
msgstr ""

#: ../qml/Components/HeaderSettings.qml:11
msgid "Information"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:45
msgid "Description"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:52 ../qml/Components/RenderUriPopUp.qml:22
msgid "Add a description"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:64
msgid "Paste an <i>otpauth://</i> URI"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:79
msgid "Paste URI"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:86 ../qml/Components/PageEditAddKey.qml:27
#: ../qml/Components/RenderUriPopUp.qml:70
msgid "Add"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:102
msgid "Or type the information"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:117
msgid "Type"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:145
msgid "<b>Issuer</b>"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:152
msgid "Who is providing the service"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:169 ../qml/Components/RenderUriPopUp.qml:32
msgid "User"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:176
msgid "Who is using the service"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:197 ../qml/Components/RenderUriPopUp.qml:37
msgid "Algorithm"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:226 ../qml/Components/RenderUriPopUp.qml:45
msgid "Password Length"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:257 ../qml/Components/RenderUriPopUp.qml:52
msgid "Interval"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:265
msgid "Password life in seconds"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:284 ../qml/Components/RenderUriPopUp.qml:58
msgid "Counter"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:291
msgid "Password initial generation"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:302 ../qml/Components/PageCheckdB.qml:172
#: ../qml/Components/PageImport.qml:172 ../qml/Components/PageKeysInfo.qml:214
msgid "Secret Key"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:309
msgid "Secret key base"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:356
msgid ""
"URI format is invalid. The expected format is 'otpauth://totp/[ID]?"
"secret=[CODE]`"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:35
msgid "Delete"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:57
msgid "Copy key"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:64
msgid "Edit key"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:88
msgid "%1 at %2"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:89
msgid "%1. %2 at %3"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:99
msgid "# %1"
msgstr ""

#: ../qml/Components/PageBackup.qml:32
msgid "Manage Backups"
msgstr ""

#: ../qml/Components/PageBackup.qml:59
msgid "Export and import keys from a file"
msgstr ""

#: ../qml/Components/PageBackup.qml:72
msgid ""
"Export all keys in one json file. Keep in mind that keys are stored as plain "
"text."
msgstr ""

#: ../qml/Components/PageBackup.qml:80
msgid ""
"It's advisable to encrypt that file. The <a href='https://open-store.io/app/"
"enigma.hummlbach'>Enigma</a> app could help here."
msgstr ""

#: ../qml/Components/PageBackup.qml:90
msgid "Export json file"
msgstr ""

#: ../qml/Components/PageBackup.qml:119
msgid "Import keys from a json file. Select a plain text json file."
msgstr ""

#: ../qml/Components/PageBackup.qml:128
msgid "Import json file"
msgstr ""

#: ../qml/Components/PageBackup.qml:159
msgid ""
"Import keys from a text file containing a list of 'otpauth://' URIs. Select "
"a plain text json file."
msgstr ""

#: ../qml/Components/PageBackup.qml:168
msgid "Import text file"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:29 ../qml/Components/PageImport.qml:29
#: ../qml/Components/PageKeysInfo.qml:32
msgid "%1 Key Information"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:65 ../qml/Components/PageImport.qml:65
#: ../qml/Components/PageKeysInfo.qml:77
msgid "%1 generator: %2 at %32"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:129 ../qml/Components/PageImport.qml:129
#: ../qml/Components/PageKeysInfo.qml:144
msgid "Generate password"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:135 ../qml/Components/PageCheckdB.qml:208
#: ../qml/Components/PageImport.qml:135 ../qml/Components/PageImport.qml:208
#: ../qml/Components/PageKeysInfo.qml:28 ../qml/Components/PageKeysInfo.qml:151
msgid "Counter: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:142 ../qml/Components/PageImport.qml:142
#: ../qml/Components/PageKeysInfo.qml:135
msgid "Copy to clipboard"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:187 ../qml/Components/PageImport.qml:187
#: ../qml/Components/PageKeysInfo.qml:229
msgid "Algorithm: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:193 ../qml/Components/PageImport.qml:193
#: ../qml/Components/PageKeysInfo.qml:235
msgid "Password Length: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:201 ../qml/Components/PageImport.qml:201
#: ../qml/Components/PageKeysInfo.qml:243
msgid "Interval: %1"
msgstr ""

#: ../qml/Components/PageEditAddKey.qml:20
msgid "Key Information"
msgstr ""

#: ../qml/Components/PageHubExport.qml:17
msgid "Export json file to"
msgstr ""

#: ../qml/Components/PageHubImport.qml:16
msgid "Import backup file from"
msgstr ""

#: ../qml/Components/PageHubImport.qml:20
msgid "Back"
msgstr ""

#: ../qml/Components/PageKeysInfo.qml:174
msgid "Complete Key Info"
msgstr ""

#: ../qml/Components/PageKeysResume.qml:26 tfamanager.desktop.in.h:1
msgid "2FA Manager"
msgstr ""

#: ../qml/Components/PageSettings.qml:43
msgid "Theme"
msgstr ""

#: ../qml/Components/PageSettings.qml:69
msgid "Backup and import"
msgstr ""

#: ../qml/Components/PageSettings.qml:89
msgid "Manage Backup"
msgstr ""

#: ../qml/Components/PageSettings.qml:90
msgid "Export, import keys and create a back up file"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:17
msgid "Add Authenticator Key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:18
msgid "%1 authenticator key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:27
msgid "Issuer"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:64
msgid "Key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:76
msgid "Cancel"
msgstr ""

#: ../qml/Main.qml:75
msgid "Add Key"
msgstr ""

#: tfamanager.desktop.in.h:2
msgid ""
"2fa;Multi-factor authentication;two factor authenticator;security;passwords;"
msgstr ""
