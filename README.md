# 2FA Manager

Small Two Factor Authenticator manager app for Ubuntu Touch.

[![OpenStore](https://open-store.io/badges/ca.svg)](https://open-store.io/app/tfamanager.cibersheep)

## Building

Clone the repo: `git clone git@gitlab.com:cibersheep/tfamanager.git`

Build with [clickable](https://clickable-ut.dev):
- Run on the desktop: `clickable desktop`
- Any architecture device attached to the computer: `clickable`

## Translating

As the translation site is down, I recommend to translate with PoEdit

## Aknoledgements

Thanks to Malte for setting up the weblate server
